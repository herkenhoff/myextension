import XCTest
@testable import MyExtension

final class MyExtensionTests: XCTestCase {
    
    func testMd5(){
        let result = "Hello".md5
        XCTAssertEqual("8b1a9953c4611296a827abf8c47804d7", result)
    }
    
    func testBase64() {
        let text = "Hello World"
        let base64 = "SGVsbG8gV29ybGQ="
        
        XCTAssertEqual(base64, text.encodeBase64!)
        XCTAssertEqual(text, base64.decodeBase64!)
    }
    
    func testFormattedDate(){
        let date = Date(year: 1, month: 1, day: 2020, hour: 10, minute: 15)
        XCTAssertEqual("10:15", date.formattedTime)
    }
    
    func testShortNameOfMonth(){
        let shortNameOfMonth = Date(year: 2020, month: 1).shortNameOfMonth
        XCTAssertEqual(shortNameOfMonth, "Jan.")
    }
    
    func testNumberFormatter() {
        let decimalNumber = NSDecimalNumber(value: 10.25)
        
        XCTAssertEqual(decimalNumber.formattedString, "10")
        XCTAssertEqual(decimalNumber.formattedString(0), "10")
        XCTAssertEqual(decimalNumber.formattedString(2), "10,25")
        XCTAssertEqual(decimalNumber.formattedString(4), "10,2500")
        
//        XCTAssertEqual(decimalNumber.currencyString, "10 €")
//        XCTAssertEqual(decimalNumber.currencyString(2), "10,25 €")
        
        XCTAssertEqual(decimalNumber.formattedPercentage, "10 %")
        XCTAssertEqual(decimalNumber.formattedPercentage(2), "10,25 %")
    }
    
    func testBetween() {
        XCTAssertTrue(5.isBetween(1, and: 10))
        XCTAssertTrue(5.isBetween(1, and: 5))
        XCTAssertTrue(1.isBetween(1, and: 5))
        XCTAssertTrue(Date(year: 2021, month: 1, day: 5).isBetween(Date(year: 2021, month: 1, day: 1), and: Date(year: 2021, month: 2, day: 1)))
        XCTAssertTrue(Date(year: 2021, month: 1, day: 1).isBetween(Date(year: 2021, month: 1, day: 1), and: Date(year: 2021, month: 2, day: 1)))
        XCTAssertTrue(Date(year: 2021, month: 2, day: 1).isBetween(Date(year: 2021, month: 1, day: 1), and: Date(year: 2021, month: 2, day: 1)))
        XCTAssertFalse(Date(year: 2022).isBetween(Date(year: 2023), and: Date(year: 2021)))
    }
    
//    func testLocalization(){
//        let cancelAction = UIAlertAction.cancelAction
//        XCTAssertEqual("Abbrechen", cancelAction.title!)
//    }
}
