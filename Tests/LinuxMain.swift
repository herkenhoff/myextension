import XCTest

import MyExtensionTests

var tests = [XCTestCaseEntry]()
tests += MyExtensionTests.allTests()
XCTMain(tests)
