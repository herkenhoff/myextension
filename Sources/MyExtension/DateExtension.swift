//
//  DateExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

import Foundation

public extension Date {
    
    init(year: Int) {
        self.init(year: year, month: 1, day: 1, hour: 0, minute: 0)
    }
    
    init(year: Int, month: Int) {
        self.init(year: year, month: month, day: 1, hour: 0, minute: 0)
    }
    
    init(year: Int, month: Int, day: Int) {
        self.init(year: year, month: month, day: day, hour: 0, minute: 0)
    }
    
    init(year: Int, month: Int, day: Int, hour: Int, minute: Int) {
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        
        let userCalendar = Calendar.current
        self = userCalendar.date(from: dateComponents)!
    }
    
    func daysTo(_ date: Date) -> Int{
        let calendar = NSCalendar.current
        
        let date1 = calendar.startOfDay(for: self)
        let date2 = calendar.startOfDay(for: date)
        
        let components = calendar.dateComponents([.day], from: date1, to: date2)
        return components.day!
    }
    
    func addMonths(_ months: Int) -> Date {
        var dc = DateComponents()
        dc.month = months
        
        return Calendar.current.date(byAdding: dc, to: self)!
    }
    
    func addDays(_ days: Int) -> Date {
        return self.addingTimeInterval(TimeInterval(days * 60 * 60 * 24))
    }
    
    func addHours(_ hours: Int) -> Date {
        return self.addingTimeInterval(TimeInterval(hours * 60 * 60))
    }
    
    func addSeconds(_ seconds: Int) -> Date {
        return self.addingTimeInterval(TimeInterval(seconds))
    }
    
    func isBetween(_ fromDate: Date, and toDate: Date) -> Bool {
        if fromDate > toDate { return false }
        return (fromDate...toDate ~= self)
    }
    
    func string(with dateFormate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormate
        
        return dateFormatter.string(from: self)
    }
    
    var year: Int {
        let calendar = Calendar.current
        return calendar.component(.year, from: self)
    }
    
    var month: Int {
        let calendar = Calendar.current
        return calendar.component(.month, from: self)
    }
    
    var day: Int {
        let calendar = Calendar.current
        return calendar.component(.day, from: self)
    }
    
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.timeStyle = .none
        return formatter.string(from: self)
    }
    
    var formattedTime: String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: self)
    }
    
    var fullFormattedDate: String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateStyle = .short
        formatter.timeStyle = .medium
        return formatter.string(from: self)
    }
    
    var date: Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: self)
        return calendar.date(from: components)!
    }
    
    var firstDayOfMonth: Date {
        return Date(year: self.year, month: self.month)
    }
    
    var lastDayOfMonth: Date {
        return Date(year: self.year, month: self.month)
            .addMonths(1)
            .addDays(-1)
    }
    
    var firstDayOfYear: Date {
        return Date(year: self.year)
    }
    
    var lastDayOfYear: Date {
        return Date(year: self.year, month: 12, day: 31)
    }
    
    var nameOfMonth: String {
        let df = DateFormatter()
        df.dateFormat = "MMMM"
        return df.string(from: self)
    }
    
    var shortNameOfMonth: String {
        let df = DateFormatter()
        df.dateFormat = "MMM"
        return df.string(from: self)
    }
    
    static var today: Date {
        let now = Date()
        return now.date
    }
    
}
