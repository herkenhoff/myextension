//
//  StringExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

import Foundation
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

public extension String {
    
    func replaceTags(_ keyValuePairs: Dictionary<String, String>, tag: String) -> String {
        var template = self
        for kvp in keyValuePairs {
            template = template.replacingOccurrences(of: "\(tag)\(kvp.key)\(tag)", with: kvp.value)
        }
        return template
    }
    
    mutating func replaceingTags(_ keyValuePairs: Dictionary<String, String>, tag: String) {
        self = self.replaceTags(keyValuePairs, tag: tag)
    }
    
    func groups(with pattern: String) -> [String]? {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
            
            guard let match = matches.first else { return nil}
            return match.groups(string: self)
        } catch {
            return nil
        }
    }
    
    @available(iOS 11.0, *)
    @available(OSX 10.13, *)
    func groups(with pattern: String, keys: [String]) -> Dictionary<String,String>? {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            let matches = regex.matches(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count))
            
            guard let match = matches.first else { return nil}
            return match.groups(string: self, keys: keys)
        } catch {
            return nil
        }
    }
    
    var fileName: String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    var fileExtension: String {
        return URL(fileURLWithPath: self).pathExtension
    }
    
    var fileContent: String? {
        let path = Bundle.main.path(forResource: self, ofType: self.fileExtension)
        return try? String(contentsOfFile: path!, encoding: .utf8)
    }
    
    func md5Data(using encoding: String.Encoding) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = self.data(using: encoding)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
    var md5: String {
        return self.md5(using: .utf8)
    }
    
    func md5(using: String.Encoding) -> String {
        return self.md5Data(using: using).map { String(format: "%02hhx", $0) }.joined()
    }
    
    var encodeBase64: String? {
        return encodeBase64(using: .utf8)
    }
    
    func encodeBase64(using: String.Encoding) -> String? {
        let encodedData = self.data(using: using)
        return encodedData?.base64EncodedString()
    }
    
    var decodeBase64: String? {
        return decodeBase64(using: .utf8)
    }
    
    func decodeBase64(using: String.Encoding) -> String? {
        guard let decodedData = Data(base64Encoded: self) else { return nil }
        return String(data: decodedData, encoding: using)
    }
    
    var intValue: Int? {
        let formatter = NumberFormatter.defaultFormatter()
        return formatter.number(from: self)?.intValue
    }
    
    var floatValue: Float? {
        let formatter = NumberFormatter.defaultFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)?.floatValue
    }
    
    var doubleValue: Double? {
        let formatter = NumberFormatter.defaultFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)?.doubleValue
    }
}
