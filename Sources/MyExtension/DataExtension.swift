//
//  DataExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

import Foundation

public extension Data {
    
    func save(_ fileName: String) -> String {
        let path: String? = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first
        let filePath = URL(fileURLWithPath: path!).appendingPathComponent(fileName)
        do {
            try write(to: filePath, options: .atomic)
        }catch {
            print(error)
        }
        return filePath.absoluteString
    }
    
}
