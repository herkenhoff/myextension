//
//  UserDefault.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 18.06.19.
//  Copyright © 2019 Herkenhoff. All rights reserved.
//

import Foundation

public extension UserDefaults {
    
    func value<T>(_ key: String, defaultValue: T) -> T {
        let userDefault = UserDefaults()
        if let value = userDefault.object(forKey: key) as? T {
            return value
        }else {
            userDefault.set(defaultValue, forKey: key)
            userDefault.synchronize()
            
            return defaultValue
        }
    }
}
