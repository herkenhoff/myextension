//
//  NumberExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

import Foundation

public extension NumberFormatter {
    
    class func defaultFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        
        formatter.numberStyle = .decimal
        
        return formatter
    }
    
    class func formatter(numberOfFactionDigits: Int) -> NumberFormatter {
        let formatter = NumberFormatter.defaultFormatter()
        
        formatter.minimumFractionDigits = numberOfFactionDigits
        formatter.maximumFractionDigits = numberOfFactionDigits
        
        return formatter
    }
    
}

public extension NSNumber {
    
    var currencyString: String {
        return currencyString(0)
    }
    
    var formattedString: String {
        return formattedString(0)
    }
    
    var formattedPercentage: String {
        return formattedPercentage(0)
    }
    
    func currencyString(_ numberOfFactionDigits: Int) -> String {
        let formatter = NumberFormatter.formatter(numberOfFactionDigits: numberOfFactionDigits)
        formatter.numberStyle = .currency
        return formatter.string(from: self)!
    }
    
    func formattedString(_ numberOfFactionDigits: Int) -> String {
        let formatter = NumberFormatter.formatter(numberOfFactionDigits: numberOfFactionDigits)
        return formatter.string(from: self)!
    }
    
    func formattedPercentage(_ numberOfFactionDigits: Int) -> String {
        let formatter = NumberFormatter.formatter(numberOfFactionDigits: numberOfFactionDigits)
        
        return "\(formatter.string(from: self)!) %"
    }
    
}

public extension Int {

    var currencyString: String {
        return currencyString(0)
    }

    var formattedString: String {
        return formattedString(0)
    }

    var formattedPercentage: String {
        return formattedPercentage(0)
    }

    func currencyString(_ numberOfFactionDigits: Int) -> String {
        return NSNumber(value: self).currencyString(numberOfFactionDigits)
    }

    func formattedString(_ numberOfFactionDigits: Int) -> String {
        return NSNumber(value: self).formattedString(numberOfFactionDigits)
    }

    func formattedPercentage(_ numberOfFactionDigits: Int) -> String {
        return NSNumber(value: self).formattedPercentage(numberOfFactionDigits)
    }
    
    func isBetween(_ a: Int, and b: Int) -> Bool {
        return (a...b ~= self)
    }

}

public extension Float {

    var currencyString: String {
        return currencyString(0)
    }

    var formattedString: String {
        return formattedString(0)
    }

    var formattedPercentage: String {
        return formattedPercentage(0)
    }

    func currencyString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).currencyString(numberOfFactionDigits)
    }

    func formattedString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).formattedString(numberOfFactionDigits)
    }

    func formattedPercentage(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).formattedPercentage(numberOfFactionDigits)
    }

}

public extension Double {

    var currencyString: String {
        return currencyString(0)
    }

    var formattedString: String {
        return formattedString(0)
    }

    var formattedPercentage: String {
        return formattedPercentage(0)
    }

    mutating func add(_ value: Double) {
        self = self.advanced(by: value)
    }

    func currencyString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).currencyString(numberOfFactionDigits)
    }

    func formattedString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).formattedString(numberOfFactionDigits)
    }

    func formattedPercentage(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(value: self).formattedPercentage(numberOfFactionDigits)
    }

}

public extension Decimal {

    var currencyString: String {
        return currencyString(0)
    }

    var formattedString: String {
        return formattedString(0)
    }

    var formattedPercentage: String {
        return formattedPercentage(0)
    }

    func currencyString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(decimal: self).currencyString(numberOfFactionDigits)
    }

    func formattedString(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(decimal: self).formattedString(numberOfFactionDigits)
    }

    func formattedPercentage(_ numberOfFactionDigits: Int) -> String {
        return NSDecimalNumber(decimal: self).formattedPercentage(numberOfFactionDigits)
    }
}
