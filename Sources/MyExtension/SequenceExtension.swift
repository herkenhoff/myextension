//
//  SequenceExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 18.06.19.
//  Copyright © 2019 Herkenhoff. All rights reserved.
//

import Foundation

public extension Sequence {
    
    func sum(_ value: (Element) -> NSDecimalNumber) -> NSDecimalNumber {
        return self.reduce(NSDecimalNumber(value: 0), {$0.adding(value($1))})
    }
    
    func sum(_ value: (Element) -> NSNumber) -> NSNumber {
        let sum = self.reduce(0.0, {$0 + value($1).doubleValue})
        return NSNumber(value: sum)
    }
    
    func avg(_ value: (Element) -> NSDecimalNumber) -> NSDecimalNumber {
        let sum = self.reduce(NSDecimalNumber(value: 0), {$0.adding(value($1))})
        
        if self.underestimatedCount == 0 { return sum }
        
        return sum.dividing(by: NSDecimalNumber(value: self.underestimatedCount))
    }
    
    var isEmpty: Bool {
        return self.underestimatedCount <= 0
    }
   
}
