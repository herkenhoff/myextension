//
//  File.swift
//  
//
//  Created by Christian on 14.04.21.
//

#if canImport(UIKit)

import UIKit

public extension UIAlertAction {
    static let okAction = UIAlertAction(title: NSLocalizedString("OK", bundle: Bundle.module, comment: ""), style: .default, handler: nil)
    
    static let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", bundle: Bundle.module, comment: ""), style: .cancel, handler: nil)
    
    static func saveAction(saveHandler:@escaping ((UIAlertAction) -> Void)) -> UIAlertAction{
        return UIAlertAction(title: NSLocalizedString("Save", bundle: Bundle.module, comment: ""), style: .default, handler: saveHandler)
    }
}

#endif
