//
//  Extension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {
    
    mutating func remove(object: Element) {
        if let index = firstIndex(of: object) {
            remove(at: index)
        }
    }
    
}

public extension Array {
    
    func any(_ element: (Element)-> Bool) -> Bool {
        return !self.filter(element).isEmpty
    }
    
    func group<GroupElement>(by criteria: (Element) -> GroupElement) -> [[Element]] where GroupElement: Comparable&Hashable {
        let groups = Dictionary(grouping: self, by: criteria)
        var outerArray = [[Element]]()
        
        for key in groups.keys.sorted() {
            outerArray.append(groups[key]!)
        }
        return outerArray
    }
    
}
