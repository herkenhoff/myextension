//
//  NSTextCheckingResultExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 21.03.19.
//  Copyright © 2019 Herkenhoff. All rights reserved.
//

import Foundation

public extension NSTextCheckingResult
{
    func groups(string: String) -> [String] {
        var groups = [String]()
        for i in  1 ..< self.numberOfRanges
        {
            if let range = Range(self.range(at: i), in: string) {
                let group = String(string[range])
                groups.append(group)
            }
        }
        return groups
    }
    
    @available(OSX 10.13, *)
    @available(iOS 11.0, *)
    func groups(string: String, keys: [String]) -> Dictionary<String,String> {
        var groups = Dictionary<String,String>()
        for key in keys {
            if let range = Range(self.range(withName: key), in: string) {
                let group = String(string[range])
                groups[key] = group
            }
        }
        
        return groups
    }
}
