//
//  UISearchBarExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 20.03.19.
//  Copyright © 2019 Herkenhoff. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public extension UISearchBar {
    
    func hideIcon() {
        let textFieldInsideSearchBar = self.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.leftViewMode = .never
    }
    
}

#endif
