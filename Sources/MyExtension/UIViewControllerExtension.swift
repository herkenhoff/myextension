//
//  UIViewController.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 08.11.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public extension UIViewController {
    
    func presentPopover(_ viewController: UIViewController, barButtonItem: UIBarButtonItem) {
        let nvc = UINavigationController(rootViewController: viewController)
        nvc.popoverPresentationController?.barButtonItem = barButtonItem
        
        self.present(nvc, animated: true, completion: nil)
    }
    
    func addAddBarButton(action: Selector?) {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: action)
    }
    
    func addCancelBarButton() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelBarButtonTouched))
    }
    
    @objc private func cancelBarButtonTouched() {
        dismiss(animated: true, completion: nil)
    }
    
    func present(_ error: Error) {
        present(UIAlertController.errorAlert(error), animated: true, completion: nil)
    }
    
    func showMessage(title: String? = nil, message: String? = nil, completionHandler:(()->Void)? = nil) {
        present(UIAlertController.messageAlert(title: title, message: message), animated: true, completion: completionHandler)
    }
    
    func showSaveAlert(title: String? = nil, message: String? = nil, saveHandler:@escaping ((UIAlertAction) -> Void), completionHandler:(()->Void)? = nil) {
        present(UIAlertController.saveAlert(title: title, message: message, saveHandler: saveHandler), animated: true, completion: completionHandler)
    }
}

#endif

//#if canImport(UIKit) && canImport(SwiftUI)
//
//import UIKit
//import SwiftUI
//
//@available(iOS 13, *)
//public extension UIViewController {
//    
//    func addSwiftUIView<T>(_ view: T) where T:View {
//        
//        let hostingView = UIHostingController(rootView: view)
//        
//        self.addChild(hostingView)
//        self.view.addSubview(hostingView.view)
//        hostingView.didMove(toParent: self)
//        
//        hostingView.view.translatesAutoresizingMaskIntoConstraints = false
//        hostingView.view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
//        hostingView.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
//        hostingView.view.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
//        hostingView.view.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
//    }
//    
//    func present<T>(_ view: T, animated: Bool, completion:(()->Void)?) where T:View {
//        let hostingView = UIHostingController(rootView: view)
//        self.present(hostingView, animated: animated, completion: completion)
//    }
//    
//}
//
//#endif
