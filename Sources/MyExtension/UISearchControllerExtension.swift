//
//  UISearchControllerExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 20.03.19.
//  Copyright © 2019 Herkenhoff. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public extension UISearchController {
    
    var searchBarIsEmpty: Bool {
        return self.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
        return self.isActive && !searchBarIsEmpty
    }
    
}

#endif
