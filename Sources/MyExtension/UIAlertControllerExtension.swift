//
//  File.swift
//  
//
//  Created by Christian on 14.04.21.
//

#if canImport(UIKit)

import UIKit

public extension UIAlertController {
    
    static func messageAlert(title: String? = nil, message: String? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(.okAction)
        
        return alert
    }
    
    static func errorAlert(_ error: Error) -> UIAlertController {
        return messageAlert(title: NSLocalizedString("An error occurred", bundle: Bundle.module, comment: ""), message: error.localizedDescription)
    }
    
    static func saveAlert(title: String? = nil, message: String? = nil, saveHandler: @escaping ((UIAlertAction)->Void)) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(.saveAction(saveHandler: saveHandler))
        alert.addAction(.cancelAction)
        
        return alert
    }
    
}

#endif
