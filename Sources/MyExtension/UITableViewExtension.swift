//
//  UITableViewExtension.swift
//  MyExtension
//
//  Created by Christian Herkenhoff on 29.09.18.
//  Copyright © 2018 Herkenhoff. All rights reserved.
//

#if canImport(UIKit)

import UIKit

public extension UITableView {
    
    func register(identifier: String) {
        self.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func footerView(_ footerData:[String], section: Int) -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 40))
        
        let textLabel = UILabel()
        textLabel.font = UIFont.boldSystemFont(ofSize: 14)
        textLabel.textColor = UIColor.gray
        textLabel.text = footerData[0].uppercased()
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        let vTextLabelConstraint = NSLayoutConstraint(item: textLabel, attribute: .top, relatedBy: .equal,
                                                      toItem: footerView, attribute: .top, multiplier: 1.0, constant: 8.0)
        let hTextLabelConstraint = NSLayoutConstraint(item: textLabel, attribute: .leading, relatedBy: .equal,
                                                      toItem: footerView, attribute: .leading, multiplier: 1.0, constant: 16.0)
        footerView.addSubview(textLabel)
        footerView.addConstraints([vTextLabelConstraint, hTextLabelConstraint])
        
        let detailTextLabel = UILabel()
        detailTextLabel.font = UIFont.boldSystemFont(ofSize: 14)
        detailTextLabel.textColor = UIColor.gray
        detailTextLabel.text = footerData[1]
        detailTextLabel.textAlignment = .right
        detailTextLabel.translatesAutoresizingMaskIntoConstraints = false
        let vDetailLabelConstraint = NSLayoutConstraint(item: detailTextLabel, attribute: .top, relatedBy: .equal,
                                                        toItem: footerView, attribute: .top, multiplier: 1.0, constant: 8.0)
        let hDetailLabelConstraint = NSLayoutConstraint(item: detailTextLabel, attribute: .trailing, relatedBy: .equal,
                                                        toItem: footerView, attribute: .trailing, multiplier: 1.0, constant: -16.0)
        footerView.addSubview(detailTextLabel)
        footerView.addConstraints([vDetailLabelConstraint, hDetailLabelConstraint])
        
        return footerView
    }
}

#endif
