//
//  File.swift
//  
//
//  Created by Christian on 14.04.21.
//

//#if canImport(UIKit) && canImport(SwiftUI)
//
//import UIKit
//import SwiftUI
//
//@available(iOS 13, *)
//extension UINavigationController {
//
//    func pushSwiftUI<T>(_ view: T, animiated: Bool) where T:View {
//        let hostingView = UIHostingController(rootView: view)
//        self.pushViewController(hostingView, animated: animiated)
//    }
//
//}
//
//#endif
